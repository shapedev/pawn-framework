#if !defined _samp_included
	#error Include a_samp before config
#endif

#if defined _config_included
	#endinput
#else
	#define _config_included
#endif

// |--------------Main Settings---------------|

#if !defined HOSTNAME
	#define HOSTNAME "Here your project name"
#endif

#if !defined GAMEMODE
	#define GAMEMODE "Here your mode name"
#endif

#if !defined mysql_file
	#define mysql_file "mysql.ini"// if you don't want use file for mysql use config/mysql.inc and settings
#endif

/*#define mysql_host		"localhost"
#define mysql_user		"root"
#define mysql_pass		""
#define mysql_dbname	""*/

// |------------------------------------------|